libb-perlreq-perl (0.82-8) unstable; urgency=medium

  * Add patch from CPAN RT for perl 5.38 compatibility. (Closes: #1040387)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Thu, 03 Aug 2023 23:18:08 +0200

libb-perlreq-perl (0.82-7) unstable; urgency=medium

  * Add patch to fix tests with perl >= 5.35.12.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.1.

 -- gregor herrmann <gregoa@debian.org>  Wed, 08 Jun 2022 17:44:42 +0200

libb-perlreq-perl (0.82-6) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Bump debhelper from old 12 to 13.

  [ gregor herrmann ]
  * Remove $Config{vendorarch} from debian/rules.
    Causes issues with cross-builds, and can be replaced by
    `find … -delete'.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Fri, 05 Nov 2021 17:34:44 +0100

libb-perlreq-perl (0.82-5) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Add patch to fix failure with perl 5.26.
    Thanks to Petr Písař <ppisar@redhat.com>.
    (Closes: #865022)
  * Set bindnow linker flag in debian/rules.
  * Update years of packaging copyright.
  * Make package autopkgtest-able.
  * Declare compliance with Debian Policy 4.0.0.

 -- gregor herrmann <gregoa@debian.org>  Mon, 19 Jun 2017 21:03:18 +0200

libb-perlreq-perl (0.82-4) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata.
  * Add patch to fix compatibility with Perl 5.22. Provided by
    ppisar@redhat.com at CPAN RT. (Closes: #787461)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Thu, 20 Aug 2015 22:39:56 +0200

libb-perlreq-perl (0.82-3) unstable; urgency=medium

  * Strip trailing slash from metacpan URLs.
  * Use $Config{vendorarch} in debian/rules as a preparation for the
    multi-arched perl 5.20.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Mon, 02 Jun 2014 20:22:07 +0200

libb-perlreq-perl (0.82-2) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Add patch from Niko Tyni to handle hash randomization changes in Perl
    since 5.17. (Closes: #708592)
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 18 May 2013 13:32:10 +0200

libb-perlreq-perl (0.82-1) unstable; urgency=low

  [ Xavier Guimard ]
  * Imported Upstream version 0.82
  * Bump Standards-Version to 3.9.4
  * Update debian/copyright (years and format)
  * Use debhelper >= 9.20120312
  * Change Architecture from "all" to "any"

  [ gregor herrmann ]
  * Refresh patch (offset).

 -- Xavier Guimard <x.guimard@free.fr>  Sat, 01 Dec 2012 22:02:49 +0100

libb-perlreq-perl (0.80-1) unstable; urgency=low

  * New upstream release.
  * Add libtry-tiny-perl to Build-Depends-Indep.

 -- gregor herrmann <gregoa@debian.org>  Wed, 07 Dec 2011 18:32:46 +0100

libb-perlreq-perl (0.78-1) unstable; urgency=low

  [ Florian Schlichting ]
  * Imported Upstream version 0.77, 0.78
  * Add myself to Uploaders.
  * Remove all perl.* scripts (questionable utility on non-RPM-based
    distributions)

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream release.
  * Drop 0001-Adapt-to-5.13.x-dependency-changes.patch, the issue is
    solved now.
  * Refresh spelling.patch (offset).

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Mon, 14 Nov 2011 21:33:00 +0000

libb-perlreq-perl (0.74-2) unstable; urgency=low

  * Add patch by Niko Tyni to work with perl 5.14 (closes: #629275).
  * Add /me to Uploaders.
  * Set Standards-Version to 3.9.2 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Sun, 10 Jul 2011 21:22:37 +0200

libb-perlreq-perl (0.74-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Standards-Version 3.9.1 (no changes)
  * Bump to debhelper 8
  * Refresh copyright information

  [ Ansgar Burchardt ]
  * Update my email address.

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 27 Feb 2011 21:52:21 -0500

libb-perlreq-perl (0.72-1) unstable; urgency=low

  * New upstream release.
    + Builds with perl 5.12.0. (Closes: #579010)
  * Use source format 3.0 (quilt).
  * debian/copyright: Formatting changes for current DEP-5 proposal.
  * debian/copyright: Update years of copyright.
  * Fix spelling error in the documentation.
    + new patch: spelling.patch
  * Bump Standards-Version to 3.8.4.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 24 Apr 2010 19:48:39 +0900

libb-perlreq-perl (0.71-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Standards-Version 3.8.3 (drop perl version dependency)
  * Use new short debhelper rules format (dh 7.0.50 for overrides)

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ryan Niebur ]
  * Update jawnsy's email address

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 27 Sep 2009 08:10:24 -0400

libb-perlreq-perl (0.70-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    -> Adds a few experimental modules (B::Walker, B::Clobbers, PerlReq::Utils)
    -> Added some macros for compilation
    -> Now handles 'leavetry' operations from B::Walker
  * Updated copyright statement
  * Added /me to uploaders
  * Standards-Version changed to 3.8.1

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

 -- Jonathan Yu <frequency@cpan.org>  Tue, 19 May 2009 15:09:38 -0400

libb-perlreq-perl (0.6.8-1) unstable; urgency=low

  * Initial Release. Closes: #494434 -- ITP

 -- Damyan Ivanov <dmn@debian.org>  Sat, 9 Aug 2008 15:32:34 +0300
